import sys
import os
import time
import pandas as pd
import numpy as np

start_time = time.time()

PATH = sys.argv[1]
rawData = pd.read_csv(os.path.join(PATH, "GenotypeMono.csv"))

def get_simu_infos(data, nb_m_sel):
    NB_REPS = max(rawData["Replicate"]) + 1
    NB_POPS = max(rawData["Population"]) + 1
    NB_MARKS = max(rawData["Marker"]) + 1
    NB_GENS = int(rawData.columns[len(rawData.columns)-1].split(" ")[2]) +1

    return [NB_REPS, NB_POPS, NB_MARKS, int(nb_m_sel), NB_GENS]


NB_MKSEL = sys.argv[2]

try:
    POPS_TO_GET = [int(i) for i in sys.argv[3].split(",")]
    print("On traite les populations suivantes : " + ", ".join([str(i) for i in POPS_TO_GET]))
except:
    if sys.argv[3] == "p":
        POPS_TO_GET = [i for i in range(get_simu_infos(rawData, NB_MKSEL)[1])]
        print("On traite toutes les populations : " + ", ".join([str(i) for i in POPS_TO_GET]))
    else:
        sys.exit("Info de populations à récupérer manquante")

try:
    MKS_TO_GET = [int(i) for i in sys.argv[4].split(",")]
    print("On traite les marqueurs suivants : " + ", ".join([str(i) for i in MKS_TO_GET]))
except:
    if sys.argv[4] == "m":
        MKS_TO_GET = [i for i in range(get_simu_infos(rawData, NB_MKSEL)[2])]
        print("On traite tous les marqueurs : " + ", ".join([str(i) for i in MKS_TO_GET]))
    else:
        sys.exit("Info de marqueurs manquante")

try:
    MOD_GENS = int(sys.argv[5])
    LST_GENS = [i for i in range(get_simu_infos(rawData, NB_MKSEL)[4]) if i%MOD_GENS==0 or i==max(range(get_simu_infos(rawData, NB_MKSEL)[4]))]
    print("On traite toutes les " + sys.argv[5] + " générations :" + ", ".join([str(i) for i in LST_GENS]))
except:
    if sys.argv[5] == "g":
        LST_GENS = [i for i in range(get_simu_infos(rawData, NB_MKSEL)[4])]
        print("On traite toutes les générations : " + ", ".join([str(i) for i in LST_GENS]))
    else:
        sys.exit("Info de générations manquantes")

NB_STEPS_HS = get_simu_infos(rawData, NB_MKSEL)[0] * len(POPS_TO_GET) * len(MKS_TO_GET) * len(LST_GENS)

def format_simu_infos(data, nb_m_sel):
    infos = get_simu_infos(data, nb_m_sel)
    print("Nb reps : " + str(infos[0]) + "\nNb pops : " + str(infos[1]) + "\nNb markers : " + str(infos[2]) + "\nNb marks Sel : " +  str(infos[3]) + "\nNb generations : " + str(infos[4]))

def check_all_marker_combinations(data, nb_m_sel):
    infos = get_simu_infos(data, nb_m_sel)

    nb_rows = len(data)

    print(len(data), infos[0]*infos[1]*infos[2]*3)

    if (nb_rows == infos[0]*infos[1]*infos[2]):
        return True
    else:
        return False

def find_heterozygotes(sample):
    res = [ind for ind, i in enumerate(sample["Genotype"]) if i.split("/")[0] != i.split("/")[1]]
    return res

def find_homozygotes(sample):
    res = [ind for ind, i in enumerate(sample["Genotype"]) if i.split("/")[0] == i.split("/")[1]]
    return res

def calcul_hs_hobs(data, nb_m_sel, NB_STEPS_HS):
    infos = get_simu_infos(data, nb_m_sel)
    nbReps = infos[0]
    nbPops = infos[1]
    nbMarks = int(infos[2])
    nbMkSel = int(infos[3])
    nbGens = infos[4]

    colGens = [i for i in data.columns if "Gen " in i]

    sel_nsel = ["Sel" if x < nbMkSel else "NSel" for x in [i for i in range(nbMarks)]]
    hobs = pd.DataFrame(columns=["Rep", "Pop", "Mk", "SelSt", "Gen", "Hobs"])
    hs = pd.DataFrame(columns=["Hs"])
    fis = pd.DataFrame(columns=["Fis"])
    q1 = pd.DataFrame(columns=["Q1"])
    my_step = 0
    for nrep in range(0, nbReps):
        for npop in range(0, nbPops):
            if npop in POPS_TO_GET:
                for nmk in range(0, nbMarks):
                    if nmk in MKS_TO_GET:
                        mysample =  data[(data["Replicate"] == nrep) & (data["Population"] == npop) & (data["Marker"] == nmk)]
                        mk_combs = mysample["Genotype"]
                        list_alleles = [i for i in np.unique("/".join(mk_combs).split("/"))]
                        heterozygotes = find_heterozygotes(mysample)
                        homozygotes = find_homozygotes(mysample)

                        genValues = pd.DataFrame(columns = ["gt1", "gt2", "value"])
                        for gen1 in mk_combs:
                            for gen2 in mk_combs:
                                valID = int(gen1.split("/")[0] == gen2.split("/")[0]) + int(gen1.split("/")[0] == gen2.split("/")[1]) + int(gen1.split("/")[1] == gen2.split("/")[0]) + int(gen1.split("/")[1] == gen2.split("/")[1])
                                ligne = {"gt1":gen1, "gt2":gen2, "value":valID}
                                genValues = genValues.append(ligne, ignore_index = True)

                        for ngen in range(0, nbGens):
                            if ngen in LST_GENS:
                                mygen = mysample[colGens[ngen]]
                                total_inds = mygen.sum()

                                ###
                                # Hobs
                                if total_inds == 0:
                                    myhobs = np.nan
                                    ligne = {"Rep":nrep, "Pop":npop, "Mk":nmk, "SelSt":sel_nsel[nmk], "Gen":ngen, "Hobs":myhobs}
                                    hobs = hobs.append(ligne, ignore_index=True)
                                else:
                                    if len(heterozygotes) >= 1:
                                        myhobs = mygen.iloc[heterozygotes].sum() / total_inds
                                        ligne = {"Rep":nrep, "Pop":npop, "Mk":nmk, "SelSt":sel_nsel[nmk], "Gen":ngen, "Hobs":myhobs}
                                        hobs = hobs.append(ligne, ignore_index=True)
                                    else:
                                        myhobs = 0
                                        ligne = {"Rep":nrep, "Pop":npop, "Mk":nmk, "SelSt":sel_nsel[nmk], "Gen":ngen, "Hobs":myhobs}
                                        hobs = hobs.append(ligne, ignore_index=True)

                                ###
                                # Hs
                                if total_inds == 0:
                                    myhs = np.nan
                                    ligne = {"Hs":myhs}
                                    hs = hs.append(ligne, ignore_index=True)
                                else:
                                    pi_pas_carres = []
                                    for allele in list_alleles:
                                        index_with_allele = [ind for ind, i in enumerate(mk_combs) if allele in i.split("/")]
                                        somme_indiv = 0
                                        for ind in index_with_allele:
                                            if ind in homozygotes: # si ce sont les homozygotes
                                                somme_indiv += 2 * mygen.iloc[ind]
                                            elif ind in heterozygotes: # si ce sont des heterozygotes
                                                somme_indiv += mygen.iloc[ind]
                                            else: # sinon...
                                                print("C'est la merde")
                                        mypi = somme_indiv / (2 * total_inds)
                                        pi_pas_carres.append(mypi)
                                    pi_carres = [i*i for i in pi_pas_carres]
                                    myhs = 1 - np.nansum(pi_carres)
                                    ligne = {"Hs":myhs}
                                    hs = hs.append(ligne, ignore_index=True)

                                ###
                                # Fis
                                if total_inds == 0:
                                    myfis = np.nan
                                    ligne = {"Fis": myfis}
                                    fis = fis.append(ligne, ignore_index=True)
                                else:
                                    try:
                                        myfis = 1 - (myhobs/myhs)
                                    except:
                                        myfis = np.nan
                                    if np.isnan(myfis):
                                        myfis = 1
                                    ligne = {"Fis": myfis}
                                    fis = fis.append(ligne, ignore_index=True)

                                ###
                                # Q1

                                sumTests = 0
                                nbId = 0
                                genNb = colGens[ngen]
                                myGenBett = mysample[["Genotype", genNb]]
                                for gen1 in mk_combs:
                                    for gen2 in mk_combs:
                                        if gen1 == gen2:
                                            gval = int(genValues[(genValues["gt1"] == gen1) & (genValues["gt2"] == gen2)]["value"])
                                            eff = int(myGenBett[myGenBett["Genotype"] == gen1][genNb]) * int((myGenBett[myGenBett["Genotype"] == gen1][genNb]) - 1)
                                            sumTests += eff * 4
                                            nbId += gval * eff
                                        elif gen1 != gen2:
                                            gval = int(genValues[(genValues["gt1"] == gen1) & (genValues["gt2"] == gen2)]["value"])
                                            eff = int(myGenBett[myGenBett["Genotype"] == gen1][genNb]) * int((myGenBett[myGenBett["Genotype"] == gen2][genNb]))
                                            sumTests += eff * 4
                                            nbId += gval * eff
                                        else:
                                            print("C'est la merde")
                                try:
                                    myQ1 = nbId / sumTests
                                except:
                                    myQ1 = np.nan
                                q1 = q1.append({"Q1":myQ1}, ignore_index=True)



                                ###
                                # Progres
                                #print("Progrès stats Pop : " + str(round((my_step/NB_STEPS_HS)*100)) + "%   \r", end='', flush=True)
                                my_step += 1
    print("Fin du calcul des stats génétiques populationnelles")


    popdata = hobs
    popdata["Hs"] = hs["Hs"]
    popdata["Fis"] = fis["Fis"]
    popdata["Q1"] = q1["Q1"]
    return(popdata)

def calcul_ht(data, nb_m_sel):
    infos = get_simu_infos(data, nb_m_sel)
    nbReps = infos[0]
    nbPops = infos[1]
    nbMarks = int(infos[2])
    nbMkSel = int(infos[3])
    nbGens = infos[4]

    colGens = [i for i in data.columns if "Gen " in i]

    nbStepsHt = nbReps * nbMarks * nbGens

    sel_nsel = ["Sel" if x < nbMkSel else "NSel" for x in [i for i in range(nbMarks)]]
    ht = pd.DataFrame(columns=["Rep", "Mk", "SelSt", "Gen", "Ht"])
    my_step = 0
    for nrep in range(0, nbReps):
        for nmk in range(0, nbMarks):
            myMks = data[(data["Replicate"] == nrep) & (data["Marker"] == nmk)]
            heterozygotes = find_heterozygotes(myMks)
            homozygotes = find_homozygotes(myMks)
            mk_combs = myMks["Genotype"]
            list_alleles = [i for i in np.unique("/".join(mk_combs).split("/"))]
            for ngen in range(0, nbGens):
                if ngen in LST_GENS:
                    myGen = myMks[["Population", "Marker", "Genotype", colGens[ngen]]]
                    total_inds = myGen[colGens[ngen]].sum()
                    if total_inds == 0:
                        ligne = {"Rep":nrep, "Mk":nmk, "SelSt":sel_nsel[nmk], "Gen":ngen, "Ht":np.nan}
                    else:
                        pi_pas_carres = []
                        for allele in list_alleles:
                            index_with_allele = [ind for ind, i in enumerate(mk_combs) if allele in i.split("/")]
                            somme_indiv = 0
                            for ind in index_with_allele:
                                if ind in homozygotes: # si ce sont les homozygotes
                                    somme_indiv += 2 * myGen.iloc[ind][colGens[ngen]]
                                elif ind in heterozygotes: # si ce sont des heterozygotes
                                    somme_indiv += myGen.iloc[ind][colGens[ngen]]
                                else: # sinon...
                                    print("C'est la merde")
                            mypi = somme_indiv / (2 * total_inds)
                            pi_pas_carres.append(mypi)
                        pi_carres = [i*i for i in pi_pas_carres]
                        myht = 1 - np.nansum(pi_carres)
                        ligne = {"Rep":nrep, "Mk":nmk, "SelSt":sel_nsel[nmk], "Gen":ngen, "Ht":myht}
                        ht = ht.append(ligne, ignore_index=True)

                # ###
                # # Progres
                print("Progrès stats Metapop : " + str(round((my_step/nbStepsHt)*100)) + "%   \r", end='', flush=True)
                my_step += 1
    print("Fin du calcul des stats génétiques métapopulationnelles")
    return(ht)

def calcul_Q2(data, nb_m_sel):
    infos = get_simu_infos(data, nb_m_sel)
    nbReps = infos[0]
    nbPops = infos[1]
    nbMarks = int(infos[2])
    nbMkSel = int(infos[3])
    nbGens = infos[4]

    nbStepsQ2 = nbReps * nbMarks * nbPops * (nbPops -1) * nbGens

    colGens = [i for i in data.columns if "Gen " in i]

    sel_nsel = ["Sel" if x < nbMkSel else "NSel" for x in [i for i in range(nbMarks)]]
    q2 = pd.DataFrame(columns=["Rep", "Mk", "SelSt", "Pop1", "Pop2", "Gen", "Q2"])
    my_step = 0
    for nrep in range(0, nbReps):
        for nmk in range(0, nbMarks):
            for pop1 in range(0, nbPops):
                for pop2 in range(0, nbPops):
                    if pop1 != pop2:
                        myMks = data[(data["Replicate"] == nrep) & (data["Marker"] == nmk) & ((data["Population"] == pop1) | (data["Population"] == pop2))]
                        for ngen in range(0, nbGens):
                            if ngen in LST_GENS:
                                sumTests = 0
                                nbId = 0
                                genNb = colGens[ngen]
                                myGenBett = myMks[["Population", "Genotype", genNb]]
                                mk_combs1 = myGenBett[myGenBett["Population"] == pop1]["Genotype"]
                                mk_combs2 = myGenBett[myGenBett["Population"] == pop2]["Genotype"]

                                genValues = pd.DataFrame(columns = ["gt1", "gt2", "value"])
                                for gen1 in mk_combs1:
                                    for gen2 in mk_combs2:
                                        valID = int(gen1.split("/")[0] == gen2.split("/")[0]) + int(gen1.split("/")[0] == gen2.split("/")[1]) + int(gen1.split("/")[1] == gen2.split("/")[0]) + int(gen1.split("/")[1] == gen2.split("/")[1])
                                        ligne = {"gt1":gen1,
                                                 "gt2":gen2,
                                                 "value":valID}
                                        genValues = genValues.append(ligne, ignore_index =
                                                         True)

                                for gen1 in mk_combs1:
                                    for gen2 in mk_combs2:
                                        gval = int(genValues[(genValues["gt1"] == gen1) & (genValues["gt2"] == gen2)]["value"])
                                        eff = int(myGenBett[(myGenBett["Population"] == pop1) & (myGenBett["Genotype"] == gen1)][genNb]) * int(myGenBett[(myGenBett["Population"] == pop2) & (myGenBett["Genotype"] == gen2)][genNb]) * 2
                                        sumTests += eff * 4
                                        nbId += gval * eff
                                try:
                                    myQ2 = nbId/sumTests
                                except:
                                    myQ2 = np.nan
                                ligne = {"Rep":nrep, "Pop1":pop1, "Pop2":pop2, "Mk":nmk, "SelSt":sel_nsel[nmk], "Gen":ngen, "Q2":myQ2}
                                q2 = q2.append(ligne, ignore_index=True)
                                # ###
                                # # Progres
                                print("Progrès stats Migration : " + str(round(((my_step/nbStepsQ2)*100))) + "%   \r", end='', flush=True)
                                my_step += 1
    print("Fin du calcul de Q2 pour les probabilités d'identité")
    return(q2)




def get_carrying_capacities(PATH):
    setFile = open(os.path.join(PATH, "setting.log"))
    myCarrs = []
    while(True):
        line = setFile.readline()
        if "Number of population:" in line:
            nbPops = int(line.strip().split(":")[1])
            line = setFile.readline()
            line = setFile.readline().strip()
            for npop in range(0, nbPops):
                line = setFile.readline().strip()
                lineSep = [i.strip() for i in line.split("|")]
                myCarrs.append(int(lineSep[3]))
            return(myCarrs)
        if not line:
            break

def calcul_demo_metapop(data, nb_m_sel, PATH):
    infos = get_simu_infos(data, nb_m_sel)
    nbReps = infos[0]
    nbPops = infos[1]
    nbGens = infos[4]

    colGens = [i for i in data.columns if "Gen " in i]
    survRate = pd.DataFrame(columns=["Rep", "Gen", "SurvRate"])
    for nrep in range(0, nbReps):
        for ngen in range(0, nbGens):
            if ngen in LST_GENS:
                nbLivePops = 0
                for npop in range(0, nbPops):
                    mySample = data[(data["Replicate"] == nrep) & (data["Population"] == npop) & (data["Marker"] == 0)]
                    tot_inds = mySample[colGens[ngen]].sum()
                    if tot_inds > 0:
                        nbLivePops += 1
            mysurv = nbLivePops/nbPops
            ligne = {"Rep":nrep, "Gen":ngen, "SurvRate":nbLivePops/nbPops}
            survRate = survRate.append(ligne, ignore_index=True)
    print("Fin du calcul des stats démographiques métapopulationnelles")
    return(survRate)

def calcul_demo_pop(data, nb_m_sel, PATH):
    infos = get_simu_infos(data, nb_m_sel)
    nbReps = infos[0]
    nbPops = infos[1]
    nbGens = infos[4]

    myCarrs = get_carrying_capacities(PATH)

    colGens = [i for i in data.columns if "Gen " in i]
    occRate = pd.DataFrame(columns=["Rep", "Pop", "Gen", "OccRate"])

    for nrep in range(0, nbReps):
        for npop in range(0, nbPops):
            for ngen in range(0, nbGens):
                if ngen in LST_GENS:
                    mySample = data[(data["Replicate"] == nrep) & (data["Population"] == npop) & (data["Marker"] == 0)]
                    tot_inds = mySample[colGens[ngen]].sum()
                    if tot_inds == 0:
                        myocc = np.nan
                    else:
                        myocc = tot_inds/myCarrs[npop]
                    ligne = {"Rep":nrep, "Pop":npop, "Gen":ngen, "OccRate":myocc}
                    occRate = occRate.append(ligne, ignore_index=True)
    print("Fin du calcul des stats démographiques populationnelles")
    return(occRate)

def calcul_migr(data, nb_m_sel, PATH):
    infos = get_simu_infos(data, nb_m_sel)
    nbReps = infos[0]
    nbPops = infos[1]
    nbGens = infos[4]

    my_step = 0

    myMigr = pd.read_csv(os.path.join(PATH, "numTransfertMigration.csv"))

    colGens = [i for i in myMigr.columns if "Gen " in i]

    migr = pd.DataFrame(columns=["Rep","Pop", "Gen", "migrEventIn", "migrNbIn", "migrEventOut", "migrNbOut"])
    for nrep in range(0, nbReps):
        for npop in range(0, nbPops):
            for ngen in LST_GENS:
                mySampleIn = myMigr[(myMigr["Replicate"] == nrep) & (myMigr["Target"] == npop)]
                if len(mySampleIn) > 0:
                    nbIndIn = mySampleIn[colGens[ngen]].sum()
                    nbEventIn = sum(mySampleIn[colGens[ngen]]>0)
                else:
                    nbIndIn = 0
                    nbEventIn = 0
                mySampleOut = myMigr[(myMigr["Replicate"] == nrep) & (myMigr["Source"] == npop)]
                if len(mySampleOut) > 0:
                    nbIndOut = mySampleOut[colGens[ngen]].sum()
                    nbEventOut = sum(mySampleOut[colGens[ngen]]>0)
                else:
                    nbIndOut = 0
                    nbEventOut = 0
                ligne = {"Rep":nrep, "Pop":npop, "Gen":(ngen+1), "migrEventIn":nbEventIn, "migrNbIn":nbIndIn, "migrEventOut":nbEventOut, "migrNbOut":nbIndOut}
                migr = migr.append(ligne, ignore_index=True)

                # ###
                # # Progres
                print("Progrès stats Migration : " + str(round((my_step/(nbReps * nbPops * nbGens))*100)) + "%   \r", end='', flush=True)
                my_step += 1
    print("Fin du calcul des stats de migration")
    return(migr)

def calcul_colo(data, nb_m_sel, PATH):
    infos = get_simu_infos(data, nb_m_sel)
    nbReps = infos[0]
    nbPops = infos[1]
    nbGens = infos[4]

    my_step = 0

    myColo = pd.read_csv(os.path.join(PATH, "numTransfertColonization.csv"))

    colGens = [i for i in myColo.columns if "Gen " in i]

    colo = pd.DataFrame(columns=["Rep","Pop", "Gen", "coloEventIn", "coloNbIn", "coloEventOut", "coloNbOut"])
    for nrep in range(0, nbReps):
        for npop in range(0, nbPops):
            for ngen in LST_GENS:
                mySampleIn = myColo[(myColo["Replicate"] == nrep) & (myColo["Target"] == npop)]
                if len(mySampleIn) > 0:
                    nbIndIn = mySampleIn[colGens[ngen]].sum()
                    nbEventIn = sum(mySampleIn[colGens[ngen]]>0)
                else:
                    nbIndIn = 0
                    nbEventIn = 0
                mySampleOut = myColo[(myColo["Replicate"] == nrep) & (myColo["Source"] == npop)]
                if len(mySampleOut) > 0:
                    nbIndOut = mySampleOut[colGens[ngen]].sum()
                    nbEventOut = sum(mySampleOut[colGens[ngen]]>0)
                else:
                    nbIndOut = 0
                    nbEventOut = 0
                ligne = {"Rep":nrep, "Pop":npop, "Gen":(ngen+1), "coloEventIn":nbEventIn, "coloNbIn":nbIndIn, "coloEventOut":nbEventOut, "coloNbOut":nbIndOut}
                colo = colo.append(ligne, ignore_index=True)

                # ###
                # # Progres
                print("Progrès stats Colonisation : " + str(round((my_step/(nbReps * nbPops * nbGens))*100)) + "%   \r", end='', flush=True)
                my_step += 1
    print("Fin du calcul des stats de colonisation")
    return(colo)


def write_gen_pop(myreshshobs, my_path):
    #resultat_genetic_pop = calcul_hs_hobs(rawData, NB_MKSEL, NB_STEPS_HS)
    myreshshobs.to_csv(os.path.join(PATH, "resultatGenPop.csv"), index=False)

def write_gen_metapop(myresht, my_path):
    #resultat_genetic_metapop = calcul_ht(rawData, NB_MKSEL)
    myresht.to_csv(os.path.join(PATH, "resultatGenMetapop.csv"), index=False)

def write_demo_pop(myresdemopop, my_path):
    myresdemopop.to_csv(os.path.join(PATH, "resultatDemoPop.csv"), index=False)

def write_demo_metapop(myresdemometapop, my_path):
    myresdemometapop.to_csv(os.path.join(PATH, "resultatDemoMetapop.csv"), index=False)

def write_migr(myresmigr, my_path):
    myresmigr.to_csv(os.path.join(my_path, "resultatMigration.csv"), index=False)

def write_colo(myrescolo, my_path):
    myrescolo.to_csv(os.path.join(my_path, "resultatColonisation.csv"), index=False)

def write_q2(myresq2, my_path):
    myresq2.to_csv(os.path.join(my_path, "resultatQ2.csv"), index=False)

statsCalcul = sys.argv[6].lower().split(",")

if("hobs" in statsCalcul or "hs" in statsCalcul or "fis" in statsCalcul or "genpop" in statsCalcul or "allgen" in statsCalcul or "all" in statsCalcul):
    myreshshobs = calcul_hs_hobs(rawData, NB_MKSEL, NB_STEPS_HS)
    write_gen_pop(myreshshobs, PATH)
    del myreshshobs

if("ht" in statsCalcul or "genmetapop" in statsCalcul or "allgen" in statsCalcul or "all" in statsCalcul):
    myresht = calcul_ht(rawData, NB_MKSEL)
    write_gen_metapop(myresht, PATH)
    del myresht

if("q2" in statsCalcul or "genpop" in statsCalcul or "allgen" in statsCalcul or "all" in statsCalcul):
    myresq2 = calcul_Q2(rawData, NB_MKSEL)
    write_q2(myresq2, PATH)
    del myresq2

if("surv" in statsCalcul or "demometapop" in statsCalcul or "alldemo" in statsCalcul or "all" in statsCalcul):
    myresdemometapop = calcul_demo_metapop(rawData, NB_MKSEL, PATH)
    write_demo_metapop(myresdemometapop, PATH)
    del myresdemometapop

if("occ" in statsCalcul or "demopop" in statsCalcul or "alldemo" in statsCalcul or "all" in statsCalcul):
    myresdemopop = calcul_demo_pop(rawData, NB_MKSEL, PATH)
    write_demo_pop(myresdemopop, PATH)
    del myresdemopop

if("migr" in statsCalcul or "allTransfert" in statsCalcul or "all" in statsCalcul):
    try:
        myresmigr = calcul_migr(rawData, NB_MKSEL, PATH)
        write_migr(myresmigr, PATH)
        del myresmigr
    except:
        print("Pas de données de migration")

if("colo" in statsCalcul or "alltransfert" in statsCalcul or "all" in statsCalcul):
    try:
        myrescolo = calcul_colo(rawData, NB_MKSEL, PATH)
        write_colo(myrescolo, PATH)
        del myrescolo
    except:
        print("Pas de données de colonisation")


#calcul_migr(rawData, NB_MKSEL, PATH)


stop_time = time.time()
print("%s secondes" % round(stop_time - start_time, 2))
